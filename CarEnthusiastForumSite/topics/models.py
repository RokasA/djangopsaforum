from django.contrib.contenttypes.models import ContentType
from django.db import models
from django.contrib.auth.models import User
from django.urls import reverse
from django.conf import settings
# Create your models here.


class Topic(models.Model):
    GENERIC = 'GENERIC'
    HELP = 'HELP'
    ADVICE = "ADVICE"
    MYCAR = "MYCAR"
    TIP = "TIP"
    INFORMATIONAL = "INFORMATIONAL"
    TOPIC_OF_POST_CHOISES = (
        (GENERIC, 'Generic'),
        (HELP, 'Help'),
        (ADVICE, 'Advice post'),
        (MYCAR, 'Question about my car'),
        (TIP, 'Tip'),
        (INFORMATIONAL, 'Informational'),
    )
    topic_of_post = models.CharField(choices=TOPIC_OF_POST_CHOISES, default=GENERIC, max_length=100)
    title = models.CharField(null=False, max_length=255)
    approved = models.BooleanField(default=False)
    text_of_post = models.TextField(null=True, max_length = 1024)
    created_by = models.ForeignKey(User, on_delete=models.CASCADE)
    likes = models.ManyToManyField(User, related_name="likes", null=True)
    dislikes = models.ManyToManyField(User, related_name="dislikes", null = True)
    updated = models.DateTimeField(auto_now=True, auto_now_add=False)
    created = models.DateTimeField(auto_now=False, auto_now_add=True)


    def __unicode__(self):
        return self.title

    def __str__(self):
        return self.title
    def get_absolute_url(self):
        return reverse("topics:article-detail", kwargs={"id": self.id})

    @property
    def comments(self):
        instance = self,
        id = instance.id
        qs = Comment.objects.filter(post = id)
        return qs




class Comment(models.Model):
    post = models.ForeignKey(Topic, on_delete=models.CASCADE)
    text = models.TextField(null=True, max_length=1024)
    created_by = models.ForeignKey(User, on_delete=models.CASCADE)
    likes = models.ManyToManyField(User, related_name="likes_comment")
    dislikes = models.ManyToManyField(User, related_name="dislikes_comment")
    updated = models.DateTimeField(auto_now=True, auto_now_add=False)
    created = models.DateTimeField(auto_now=False, auto_now_add=True)
    parent = models.ForeignKey("self", null=True, blank=True, on_delete=models.CASCADE)

    class Meta:
        ordering = ['-created']

    def __unicode__(self):
        return str(self.created_by.username)

    def __str__(self):
        return str(self.created_by.username)

    def get_absolute_url(self):
        return reverse("topics:thread", kwargs={"id": self.id})

    def get_delete_url(self):
        return reverse("topics:deleteComment", kwargs={"id": self.id})

    def children(self):  # replies
        return Comment.objects.filter(parent=self)

    @property
    def is_parent(self):
        if self.parent is not None:
            return False
        return True