from django.contrib.auth.decorators import login_required
from django.http import HttpResponseRedirect
from django.shortcuts import render
from django.views.generic import UpdateView
from django.contrib import messages
from django.urls import reverse_lazy
from django.views.generic.edit import DeleteView
from .forms import PostForm, CommentForm
from django.db.models import Q
from django.http import HttpResponse, HttpResponseRedirect, Http404
from django.shortcuts import render, get_object_or_404, redirect
from django.utils import timezone
from .models import Topic, Comment
# Create your views here.

@login_required(login_url='/account/login')
def post_create(request):
    form = PostForm(request.POST or None)
    if form.is_valid():
        instance = form.save(commit=False)
        instance.created_by = request.user
        if request.user.is_staff or request.user.is_superuser:
            instance.approved = True
        instance.save()
        return HttpResponseRedirect(instance.get_absolute_url())
    context = {
        "form": form,
    }
    return render(request, 'post_form.html', context)

login_url='/account/login'
def post_detail(request, id = None):
    instance = get_object_or_404(Topic, id = id)
    initial_data = {
        "post": instance,
        'created_by': request.user
    }
    form = CommentForm(request.POST or None, initial = initial_data)
    if form.is_valid():
        parent_obj = None
        content_data = form.cleaned_data.get('text')
        try:
            parent_id = int(request.POST.get("parent_id"))
        except:
            parent_id = None

        if parent_id:
            parent_qs = Comment.objects.filter(id=parent_id)
            if parent_qs.exists() and parent_qs.count()==1:
                parent_obj=parent_qs.first()
        new_comment, created = Comment.objects.get_or_create(
            post = instance,
            text = content_data,
            created_by = request.user,
            parent=parent_obj,)
    comments = Comment.objects.filter(post = instance.id)
    context = {
        'instance' : instance,
        "comments": comments,
        'comment_form': form,
    }

    return render(request, 'post_detail.html', context)

def post_list(request):
    queryset = Topic.objects.filter(approved = True)
    context = {
        'topic_list': queryset,
    }

    return render (request, 'post_list.html', context)

@login_required()
def post_update(request, id = None):
    instance = get_object_or_404(Topic, id = id)
    form = PostForm(request.POST or None, instance=instance)
    if(form.is_valid()):
        instance = form.save(commit=False)
        instance.save()
        return HttpResponseRedirect(instance.get_absolute_url())
    context = {
        "instance": instance,
        "form": form
    }
    return render(request, 'post_form.html', context)

@login_required(login_url='/account/login')
def deleteComment(request, id = None):
    try:
        obj = Comment.objects.get(id=id)
    except:
        raise Http404
    post = obj.post
    if request.method == "POST":
        parent_obj_url = post.get_absolute_url()
        obj.delete()
    # messages.success(request, "This has been deleted.")
        return HttpResponseRedirect(post.get_absolute_url())
    context = {
        "object": obj
    }
    return render(request, "confirm_delete.html", context)

@login_required(login_url='/account/login')
def comment_thread(request, id = None):
    try:
        obj = Comment.objects.get(id=id)
    except:
        raise Http404

    if not obj.is_parent:
        obj = obj.parent

    content_object = obj.post
    created_by = obj.created_by

    initial_data = {
        "post": content_object,
        'created_by': created_by
    }
    form = CommentForm(request.POST or None, initial=initial_data)
    if form.is_valid():
        parent_obj = None
        content_data = form.cleaned_data.get('text')
        try:
            parent_id = int(request.POST.get("parent_id"))
        except:
            parent_id = None

        if parent_id:
            parent_qs = Comment.objects.filter(id=parent_id)
            if parent_qs.exists() and parent_qs.count()==1:
                parent_obj=parent_qs.first()
        new_comment, created = Comment.objects.get_or_create(
            post=content_object,
            text=content_data,
            created_by= created_by,
            parent=parent_obj, )

        return HttpResponseRedirect(obj.get_absolute_url())

    context = {
        "comment": obj,
        "form": form,
    }
    return render(request, "comment_thread.html", context)

class PostDelete(DeleteView):
    model = Topic
    success_url = reverse_lazy('topics')