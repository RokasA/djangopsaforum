from django.conf.urls import url
from django.contrib import admin
from django.urls import path
from .views import post_create, post_detail, post_list, post_update,comment_thread, deleteComment, PostDelete


app_name = 'topics'
urlpatterns = [
    path('', post_list, name='posts'),
    path('create/', post_create, name='create'),
    path('<int:id>/', post_detail, name='article-detail'),
    path('update/<int:id>/', post_update, name='update'),
    path('comment/<int:id>/', comment_thread, name="thread"),
    path('comment/delete/<int:id>/', deleteComment, name="deleteComment"),

]