from django import forms
from .models import Topic


class PostForm(forms.ModelForm):
    class Meta:
        model = Topic
        fields = [
            'title',
            'text_of_post',
            'topic_of_post',
        ]

class CommentForm(forms.Form):
    text = forms.CharField(label='', widget=forms.Textarea)

