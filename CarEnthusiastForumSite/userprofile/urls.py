from django.contrib.auth import logout
from django.urls import path
from . import views

app_name = 'userprofile'

urlpatterns = [
    path('login',views.login_view,name='login'),
    path('logout',views.logout_view,name='logout'),
    path('register',views.register,name = "register"),
]