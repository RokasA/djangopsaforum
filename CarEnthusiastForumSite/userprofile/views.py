from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User
from django.http import Http404
from django.shortcuts import render, redirect

# Create your views here.
from django.urls import reverse_lazy

from userprofile.forms import UserLoginForm, UserRegistrationForm


def login_view(request):
    form = UserLoginForm(request.POST or None)
    if form.is_valid():
        username = form.cleaned_data.get("username")
        password = form.cleaned_data.get("password")
        user = authenticate(username=username,password=password)
        login(request,user)
        return render(request,'index.html')


    return render(request,"login.html",{'form':form})



def register(request):
    registered = False
    if request.method == "POST":
        user_form = UserRegistrationForm(request.POST)
        if user_form.is_valid():
            user = user_form.save(commit=False)
            password = user_form.cleaned_data.get('password1')
            user.set_password(password)
            user.save()
            new_user = authenticate(username=user.username,password=password)
            login(request,new_user)
            registered = True
            return render(request,'index.html')
        else:
            print(user_form.errors)
    else:
        user_form = UserRegistrationForm()

    args = {'user_form':user_form,
            'registered':registered}
    return render(request,'register_form.html',args)


def logout_view(request):
    logout(request)
    return redirect("/")


def make_administrator(request, id):
    try:
        user = User.objects.get(id=id)
    except:
        raise Http404
    if request.user.is_superuser:
        if user.is_staff == False:
            user.is_staff = True
    return



