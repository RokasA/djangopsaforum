from django.db import models

from django.contrib.auth.models import User
# Create your models here.
from django.db.models.signals import post_save


class UserProfile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    phonenumber = models.IntegerField(blank=True,null=True, max_length=15)
    bio = models.TextField(blank=True,null=True, max_length=255)
    picture = models.ImageField()

    def __str__(self):
        return self.user.username


def create_profile(sender, **kwargs):
    if kwargs['created']:
        user_profile = UserProfile.objects.create(user=kwargs['instance'])

post_save.connect(create_profile,sender=User)